import socket
import os
from mpi4py import MPI

def main():
    hostname = socket.gethostname()
    comm = MPI.COMM_WORLD
    rank = comm.Get_rank()
    size = comm.Get_size()
    TAG = 40
    try:
        if rank == 0:
            token = "Bonjour"
            comm.send(token, dest=1, tag=TAG)
        elif rank == 1:
            token = comm.recv(source=0, tag=TAG)
    except MPI.Exception as ierr:
        print(ierr.Get_error_string())

    print(f"{hostname}> {rank}/{size - 1}: {token}")


if __name__ == "__main__":
    main()
