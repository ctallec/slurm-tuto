#!/bin/bash
#SBATCH -N 2
#SBATCH --gres=gpu:1

source activate torch
NCCL_SOCKET_IFNAME=eno1 srun python script.py
