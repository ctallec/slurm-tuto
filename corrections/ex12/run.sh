#!/bin/bash
#SBATCH -N 2
#SBATCH --ntasks-per-node 4
#SBATCH --gres=gpu:4
#SBATCH -p besteffort
#SBATCH --output=logs/%j.stdout
#SBATCH --error=logs/%j.stderr

source activate torch
NCCL_SOCKET_IFNAME=eno1 srun python main.py --logdir logs/exp
