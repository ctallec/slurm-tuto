from collections import namedtuple
import os
import subprocess
import socket

import torch

DistrInfo = namedtuple('DistrInfo', ['master_addr',
                                     'master_port',
                                     'world_size',
                                     'local_rank',
                                     'global_rank',
                                     'n_nodes',
                                     'node_id',
                                     'n_gpus_per_node'])


def init_distributed():
    # debug, dump SLURM variables
    assert 'SLURM_JOB_ID' in os.environ, "only slurm launch are allowed"
    job_id = os.environ['SLURM_JOB_ID']
    node_id = os.environ['SLURM_NODEID']
    rank = os.environ['SLURM_PROCID']
    hostnames = subprocess.check_output(['scontrol', 'show', 'hostnames', os.environ['SLURM_JOB_NODELIST']])
    master_node = hostnames.split()[0].decode('utf-8')
    distr_info = DistrInfo(
        master_addr=master_node,
        master_port=10001,
        n_nodes=int(os.environ['SLURM_JOB_NUM_NODES']),
        node_id=int(os.environ['SLURM_NODEID']),
        local_rank=int(os.environ['SLURM_LOCALID']),
        global_rank=int(os.environ['SLURM_PROCID']),
        world_size=int(os.environ['SLURM_NTASKS']),
        n_gpus_per_node=int(os.environ['SLURM_NTASKS']) // int(os.environ['SLURM_JOB_NUM_NODES']))
    print(f"SLURM info: master_node={distr_info.master_addr}, master_port={distr_info.master_port}")
    print(f"SLURM info: id={job_id}, n_nodes={node_id}, rank={rank}")


    # set environment variables for env:// init
    os.environ['MASTER_ADDR'] = distr_info.master_addr
    os.environ['MASTER_PORT'] = str(distr_info.master_port)
    os.environ['WORLD_SIZE'] = str(distr_info.world_size)
    os.environ['RANK'] = str(distr_info.global_rank)

    # summary
    PREFIX = "%i - " % distr_info.global_rank
    print(PREFIX + "Number of nodes: %i" % distr_info.n_nodes)
    print(PREFIX + "Node ID        : %i" % distr_info.node_id)
    print(PREFIX + "Local rank     : %i" % distr_info.local_rank)
    print(PREFIX + "Global rank    : %i" % distr_info.global_rank)
    print(PREFIX + "World size     : %i" % distr_info.world_size)
    print(PREFIX + "GPUs per node  : %i" % distr_info.n_gpus_per_node)
    print(PREFIX + "Hostname       : %s" % socket.gethostname())

    # set GPU device
    torch.cuda.set_device(distr_info.local_rank)

    print("Initializing PyTorch distributed ...")
    torch.distributed.init_process_group(
        init_method='env://',
        backend='nccl',
    )

    return distr_info
