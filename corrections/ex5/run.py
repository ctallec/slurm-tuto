import argparse
from uuid import uuid4
from subprocess import call
from os.path import join, exists
from os import makedirs

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--logdir", required=True, type=str)
    args = parser.parse_args()

    if not exists(args.logdir):
        makedirs(args.logdir)

    slurm_script_fname = join('/tmp', str(uuid4()) + ".slurm")
    slurm_template = open('corrections/ex5/template.slurm', 'r').read().format(logdir=args.logdir)
    open(slurm_script_fname, 'w').write(slurm_template)
    call(['sbatch', slurm_script_fname])
