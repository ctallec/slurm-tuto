from typing import Dict, Any
import argparse
from uuid import uuid4
from subprocess import call
from os.path import join, exists
from os import makedirs
from material.utils import args2str

def generate_batch_file(
        cmd: str, exp_name: str, logdir: str,
        cmd_args: Dict[str, Any], ngpus: int,
        ncpus: int) -> str:
    full_cmd = cmd + f" --logdir {logdir} " + " ".join([args2str(k, v) for k, v in cmd_args.items()])

    return open('corrections/ex6/template.slurm', 'r').read().format(
        logdir=logdir, cmd=full_cmd, ngpus=ngpus, ncpus=ncpus)


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--logdir", required=True, type=str)
    args = parser.parse_args()

    if not exists(args.logdir):
        makedirs(args.logdir)

    slurm_script_fname = join('/tmp', str(uuid4()) + ".slurm")
    slurm_template = generate_batch_file(
        cmd="python material/script.py",
        exp_name="exp1",
        logdir=args.logdir,
        cmd_args={"p": 1},
        ngpus=1,
        ncpus=1)
    print(slurm_template)

    open(slurm_script_fname, 'w').write(slurm_template)
    call(['sbatch', slurm_script_fname])
