from typing import Dict, Any, List
import argparse
from uuid import uuid4
from subprocess import call
from os.path import join
from os import makedirs
from material.utils import args2str
import numpy as np
import time

def generate_args_list() -> List[Dict[str, Any]]:
    return [{'p': p} for p in np.linspace(0, 10, 100)]

def generate_batch_file(
        cmd: str, exp_name: str, logdir: str,
        cmd_args: Dict[str, Any], ngpus: int,
        ncpus: int, preview: bool=False) -> str:
    full_cmd = cmd + " " + " ".join([args2str(k, v) for k, v in cmd_args.items()])
    if preview:
        print(full_cmd)

    return open('corrections/ex7/template.slurm', 'r').read().format(
        logdir=logdir, cmd=full_cmd, ngpus=ngpus, ncpus=ncpus)

def launch_job_list(
        cmd: str, exp_name: str, cmd_args: List[Dict[str, Any]], ngpus: int,
        ncpus: int, preview: bool=False) -> None:
    logdir = join('logs', exp_name, time.strftime("%Y_%m_%d_%H_%M_%S"))
    if not preview:
        makedirs(logdir)
    for args in cmd_args:
        slurm_script_fname = join('/tmp', str(uuid4()) + ".slurm")
        slurm_template = generate_batch_file(
            cmd=cmd,
            exp_name=exp_name,
            logdir=logdir,
            cmd_args=args,
            ngpus=ngpus,
            ncpus=ncpus,
            preview=preview
        )
        if not preview:
            open(slurm_script_fname, 'w').write(slurm_template)
            call(['sbatch', slurm_script_fname])


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_name", required=True, type=str)
    args = parser.parse_args()

    arg_list = generate_args_list()
    # first preview
    launch_job_list(
        'python corrections/ex3/script.py',
        exp_name=args.exp_name,
        cmd_args=arg_list,
        ngpus=1,
        ncpus=1,
        preview=True)
    # then launch
    launch_job_list(
        'python corrections/ex3/script.py',
        exp_name=args.exp_name,
        cmd_args=arg_list,
        ngpus=1,
        ncpus=1,
        preview=False)
