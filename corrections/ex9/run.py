from typing import Dict, Any, List
import argparse
from uuid import uuid4
from subprocess import call
from os.path import join
from os import makedirs
from material.utils import args2str
from itertools import product
import numpy as np
import time

def launch_job(
        cmd: str, exp_name: str, cmd_args: List[Dict[str, Any]], 
        grid_args: List[Dict[str, Any]], ngpus: int,
        ncpus: int, narray: int, preview: bool=False) -> None:
    logdir = join('logs', exp_name, time.strftime("%Y_%m_%d_%H_%M_%S"))
    print(logdir)
    if not preview:
        makedirs(logdir)
    slurm_script_fname = join('/tmp', str(uuid4()) + ".slurm")
    slurm_template = generate_batch_file(
        cmd=cmd,
        exp_name=exp_name,
        logdir=logdir,
        cmd_args=cmd_args,
        grid_args=grid_args,
        ngpus=ngpus,
        ncpus=ncpus,
        narray=narray,
        preview=preview
    )
    if not preview:
        open(slurm_script_fname, 'w').write(slurm_template)
        call(['sbatch', slurm_script_fname])

def generate_batch_file(
        cmd: str, exp_name: str, logdir: str,
        cmd_args: Dict[str, Any], grid_args: Dict[str, Any], ngpus: int, 
        ncpus: int, narray: int, preview: bool=False) -> str:
    full_cmd = cmd + " " + " ".join([args2str(k, v) for k, v in cmd_args.items()])
    if preview:
        print(grid_args)
        print(full_cmd)

    return open('corrections/ex9/template.slurm', 'r').read().format(
        logdir=logdir, cmd=full_cmd, ngpus=ngpus,
        ncpus=ncpus, narray=narray, **grid_args)

if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--exp_name", required=True, type=str)
    parser.add_argument('--p', nargs='+', type=str, help='p param', required=True)
    parser.add_argument('--q', nargs='+', type=str, help='q param', required=True)
    args = parser.parse_args()
    narray = len(list(product(args.q, args.p)))-1
    arg_list = {'q': ' '.join(args.q), 'p': ' '.join(args.p)}
    # first preview
    launch_job(
        'python material/script2.py',
        exp_name=args.exp_name,
        cmd_args={},
        grid_args=arg_list,
        ngpus=1,
        ncpus=1,
        narray=narray,
        preview=True)
    # then launch
    launch_job(
        'python material/script2.py',
        exp_name=args.exp_name,
        cmd_args={},
        grid_args=arg_list,
        ngpus=1,
        ncpus=1,
        narray=narray,
        preview=False)
