Run a few srun commands to get a feel of how to reserve an interactive session on the cluster with various
requirements. Notably, try to reserve GPU ressources, and verify that the ressource is correctly allocated
by displaying the environment variable CUDA_VISIBLE_DEVICES.
