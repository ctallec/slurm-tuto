from typing import Any

def args2str(k: str, v: Any) -> str:
    if isinstance(v, bool):
        if v:
            return f"--{k}"
        return ""
    return f"--{k} {v}"
